import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ResultsComponent } from './results/results.component';
import { SearchComponent } from './search/search.component';
import { AboutComponent } from './about/about.component';

const appRoutes: Routes = [
  { path: 'results', component: ResultsComponent },
  { path: 'about', component: AboutComponent },
  { path: '', component: SearchComponent },
  { path: '**', component: SearchComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ResultsComponent,
    SearchComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
